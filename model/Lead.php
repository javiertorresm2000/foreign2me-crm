<?php
    class Lead{
        private $db;
        private $result;
        
        private $response = array(
			"status" => "",
			"body" => "",
			"data" => ""
        );

		public function __construct() {
			require_once '../config/Config_db.php';
			$class = new Connection();
			$this->db = $class->conn();
        }
        
        public function insert_new_lead_student($data){
            try{
                $name               = $data['name'];
                $last_name          = $data['last_name'];
                $email              = $data['email'];
            $phone                  = $data['phone'];
                $message            = $data['message'];


                $sql = "INSERT INTO 
                            lead_student
                        VALUES(
                            null,
                            :name,
                            :last_name,
                            :email,
                            :phone,
                            :message,
                            NOW()
                            )";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":name",$name, PDO::PARAM_STR);
                $sql->bindParam(":last_name",$last_name, PDO::PARAM_STR);
                $sql->bindParam(":email",$email, PDO::PARAM_STR);
                $sql->bindParam(":phone",$phone, PDO::PARAM_STR);
                $sql->bindParam(":message",$message, PDO::PARAM_STR);
                $sql->execute();
                $this->response['data'] = $this->db->lastInsertId();
                $this->response['status'] = 'ok';
                
            }catch (PDOException $e){
				$this->response['status'] = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->response;
        }
    }
?>