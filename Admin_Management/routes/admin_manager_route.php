<?php
	switch($_GET['controller']){
        case 'insert_new_admin':{
			require_once '../controller/admin_manager_controller.php';
			$controller = new Admins_Manager();
			$controller->insert_new_admin($_POST['data']);
			break;
        }
        
        case 'select_all_admins':{
			require_once '../controller/admin_manager_controller.php';
			$controller = new Admins_Manager();
			$controller->select_all_admins();
			break;
		}

		case 'select_specific_admin':{
			require_once '../controller/admin_manager_controller.php';
			$controller = new Admins_Manager();
			$controller->select_specific_admin($_POST['id']);
			break;
		}

		case 'update_specific_admin':{
			require_once '../controller/admin_manager_controller.php';
			$controller = new Admins_Manager();
			$controller->update_specific_admin($_POST['data']);
			break;
		}

		
	}
?>