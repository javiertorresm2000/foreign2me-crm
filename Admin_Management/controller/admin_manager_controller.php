<?php
    class Admins_Manager{
        private $response;
        

        public function __construct(){
			require_once '../model/Admin_Manager.php';
			$this->model = new Admin_Manager();
        }
        
        public function insert_new_admin($data){
            $this->response	= $this->model->insert_new_admin($data);
            
	    	echo json_encode($this->response);
        }

        public function select_all_admins(){
            $this->response	= $this->model->select_all_admins();
            
	    	echo json_encode($this->response);
        }

        public function select_specific_admin($id){
            $this->response	= $this->model->select_specific_admin($id);
            
	    	echo json_encode($this->response);
        }

        public function update_specific_admin($data){
            $this->response	= $this->model->update_specific_admin($data);
            
	    	echo json_encode($this->response);
        }


    }
?>