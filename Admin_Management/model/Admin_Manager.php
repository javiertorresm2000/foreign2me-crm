<?php
    class Admin_Manager{
        private $db;
        
        private $response = array(
			"status" => "",
			"body" => "",
			"data" => ""
        );

		public function __construct() {
			require_once '../config/Config_db.php';
			$class = new Connection();
			$this->db = $class->conn();
        }
        
        public function insert_new_admin($data){
            try{
                $admin_email              = $data['admin_email'];
                $admin_password           = $data['admin_password'];
                $admin_name              = $data['admin_name'];
                $admin_lastname           = $data['admin_lastname'];
                $admin_role              = $data['admin_role'];
                $admin_birth           = $data['admin_birth'];

                $sql = "INSERT INTO
                            admin_user
                        VALUES(
                            null,
                            :admin_email,
                            :admin_password,
                            :admin_name,
                            :admin_lastname,
                            :admin_role,
                            1,
                            0,
                            0,
                            null,
                            :admin_birth,
                            NOW(),
                            NOW())";

                $sql = $this->db->prepare($sql);

                
                $sql->bindParam(":admin_email",$admin_email, PDO::PARAM_STR);
                $sql->bindParam(":admin_password",$admin_password, PDO::PARAM_STR);
                $sql->bindParam(":admin_name",$admin_name, PDO::PARAM_STR);
                $sql->bindParam(":admin_lastname",$admin_lastname, PDO::PARAM_STR);
                $sql->bindParam(":admin_role",$admin_role, PDO::PARAM_INT);
                $sql->bindParam(":admin_birth",$admin_birth, PDO::PARAM_STR);
                $sql->execute();

                $this->response['body'] = 'Registered!';
                $this->response['status'] = 'ok';
                
            }catch (PDOException $e){
				$this->response['status'] = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->response;
        }

        public function select_all_admins(){
            try{
                $sql = "SELECT * FROM admin_user";

                $sql = $this->db->prepare($sql);
                $sql->execute();

                $this->response['data'] = $sql->fetchAll(PDO::FETCH_ASSOC);
                $this->response['body'] = 'Data Found!';
                $this->response['status'] = 'ok';
                
            }catch (PDOException $e){
				$this->response['status'] = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->response;
        }

        public function select_specific_admin($id){
            try{
                $sql = "SELECT * FROM admin_user WHERE id_admin_user = :id";

                $sql = $this->db->prepare($sql);
                $sql->bindParam(":id",$id, PDO::PARAM_INT);
                $sql->execute();

                $this->response['data'] = $sql->fetch(PDO::FETCH_ASSOC);
                $this->response['body'] = 'Data Found!';
                $this->response['status'] = 'ok';
                
            }catch (PDOException $e){
				$this->response['status'] = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->response;
        }

        public function update_specific_admin($data){
            try{
                $id_admin_user             = $data['admin_id'];
                $admin_email              = $data['admin_email'];
                $admin_password           = $data['admin_password'];
                $admin_name              = $data['admin_name'];
                $admin_lastname           = $data['admin_lastname'];
                $admin_role              = $data['admin_role'];
                $admin_birth           = $data['admin_birth'];
                $admin_status           = $data['admin_status'];
                $admin_attempts           = $data['admin_attempts'];

                $sql = "UPDATE
                            admin_user
                        SET
                            admin_email = :admin_email,
                            admin_password = :admin_password,
                            admin_name = :admin_name,
                            admin_lastname = :admin_lastname,
                            admin_role = :admin_role,
                            admin_status = :admin_status,
                            admin_attempts = :admin_attempts,
                            admin_birth = :admin_birth,
                            admin_last_update = NOW()
                        WHERE 
                            id_admin_user = :id_admin_user";

                $sql = $this->db->prepare($sql);

                
                $sql->bindParam(":admin_email",$admin_email, PDO::PARAM_STR);
                $sql->bindParam(":admin_password",$admin_password, PDO::PARAM_STR);
                $sql->bindParam(":admin_name",$admin_name, PDO::PARAM_STR);
                $sql->bindParam(":admin_lastname",$admin_lastname, PDO::PARAM_STR);
                $sql->bindParam(":admin_role",$admin_role, PDO::PARAM_INT);
                $sql->bindParam(":admin_birth",$admin_birth, PDO::PARAM_STR);
                $sql->bindParam(":admin_status",$admin_status, PDO::PARAM_INT);
                $sql->bindParam(":admin_attempts",$admin_attempts, PDO::PARAM_INT);
                $sql->bindParam(":id_admin_user",$id_admin_user, PDO::PARAM_INT);
                $sql->execute();

                $this->response['body'] = 'Registered!';
                $this->response['status'] = 'ok';
                
            }catch (PDOException $e){
				$this->response['status'] = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->response;
        }
    }
?>