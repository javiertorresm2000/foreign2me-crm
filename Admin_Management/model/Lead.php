<?php
    class Lead{
        private $db;
        
        private $response = array(
			"status" => "",
			"body" => "",
			"data" => ""
        );

		public function __construct() {
			require_once '../config/Config_db.php';
			$class = new Connection();
			$this->db = $class->conn();
        }
        

        public function select_all_students_leads(){
            try{
                $sql = "SELECT * FROM lead_student";

                $sql = $this->db->prepare($sql);
                $sql->execute();

                $this->response['data'] = $sql->fetchAll(PDO::FETCH_ASSOC);
                $this->response['body'] = 'Data Found!';
                $this->response['status'] = 'ok';
                
            }catch (PDOException $e){
				$this->response['status'] = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->response;
        }
    }
?>