
window.onload = function(){
    
    select_all_admins();
};

$("#btn_save_new_admin").click(function(){

    data = {
        admin_name : $("#name_new_admin").val(),
        admin_lastname : $("#lastname_new_admin").val(),
        admin_email : $("#email_new_admin").val(),
        admin_password : $("#password_new_admin").val(),
        admin_birth : $("#date_birt_new_admin").val(),
        admin_role : $("#role_new_admin").val(),
    };

    insert_new_admin(data);
});

function clean_form_new_admin(){
    $("#name_new_admin").val("");
    $("#lastname_new_admin").val("");
    $("#email_new_admin").val("");
    $("#password_new_admin").val("");
    $("#date_birt_new_admin").val("");
    $("#role_new_admin").val("");
}

// FUNCION PARA EDITAR UN ADMINISTRADOR
function edit_admin_user(id){
    $("#modal_edit_adminuser").modal("show");
    select_specific_adminuser(id);
    clean_modal_edit_adminuser();
}

function clean_modal_edit_adminuser(){

}

$("#btn_edit_admin").click(function (){
    data = {
        admin_id : id_edit_admin_user,
        admin_name: $("#edit_admin_name").val(),
        admin_lastname : $("#edit_admin_lastname").val(),
        admin_email : $("#edit_admin_email").val(),
        admin_password : $("#edit_admin_password").val(),
        admin_role : $("#edit_admin_role").val(),
        admin_status : $("#edit_admin_status").val(),
        admin_birth: $("#edit_admin_date_birth").val(),
        admin_attempts : $("#edit_admin_login_attempts").val(),
    };
    update_specific_adminuser(data);
});