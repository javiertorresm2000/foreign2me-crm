
var urlActual = "http://localhost/foreign2me_crm/Admin_Management/";

// LISTAR ADMINISTRADORES
function select_all_leads() {
    $.ajax({
        url: urlActual + "routes/?route=lead_route&controller=select_all_students_leads",
        type: "POST",
        dataType: "JSON",
        beforeSend: function(){
            
        },
        success: function (response){
        
            $("#body_table_student_leads").html("");
            $("#table_student_leads").DataTable().clear();
            $("#table_student_leads").DataTable().destroy();

            for (var i in response.data) {
                var row = response.data[i];
                $("#body_table_student_leads").append(
                    "<tr>" +
                        "<td>" + row.name + "</td>" +
                        "<td>" + row.last_name + "</td>" +
                        "<td>" + row.email + "</td>" +
                        "<td>" + row.phone + "</td>" +
                        "<td>" + row.message + "</td>" +
                        "<td>" + row.date_register + "</td>" +
                    "</tr>"
                );
            }
            if (!$.fn.DataTable.isDataTable("#table_student_leads")) {
                $('#table_student_leads').DataTable({
                    destroy: true,
                        processing: true,
                        scrollY: "400px",
                        sScrollX: "100%",
                        sScrollXInner: "100%",
                        bScrollCollapse: true,
                        scrollCollapse: true,
                    language: {
                        lengthMenu: "Mostrando _MENU_ registros por pagina",
                        zeroRecords: "Registros no encontrados",
                        info: "Mostrando _PAGE_ paginas de _PAGES_",
                        infoEmpty: "Registros no disponibles",
                        infoFiltered: "(filtered from _MAX_ total records)"
                    }
                });
            }
            $("#table_student_leads").DataTable().draw();
        }
    });
}
  // LISTAR ADMINISTRADORES
