
var urlActual = "http://localhost/foreign2me_crm/Admin_Management/";
var id_edit_admin_user;

// LISTAR ADMINISTRADORES
function select_all_admins() {
    $.ajax({
        url: urlActual + "routes/?route=admin_manager_route&controller=select_all_admins",
        type: "POST",
        dataType: "JSON",
        beforeSend: function(){
            
        },
        success: function (response){
        
            $("#body_table_admins").html("");
            $("#table_admins").DataTable().clear();
            $("#table_admins").DataTable().destroy();

            for (var i in response.data) {
                var row = response.data[i];

                switch(row.admin_role){
                    case '1':{
                        row.admin_role="Master";
                        break;
                    }
                    case '2':{
                        row.admin_role="High";
                        break;
                    }
                    case '3':{
                        row.admin_role="Medium";
                        break;
                    }
                    case '4':{
                        row.admin_role="Low";
                        break;
                    }
                }
                $("#body_table_admins").append(
                    '<tr class="datatable_row" onclick="edit_admin_user('+row.id_admin_user +')">' +
                        "<td>" + row.admin_name + "</td>" +
                        "<td>" + row.admin_lastname + "</td>" +
                        "<td>" + row.admin_email + "</td>" +
                        "<td>" + row.admin_role + "</td>" +
                        "<td>" + row.admin_status + "</td>" +
                        "<td>" + row.admin_attempts + "</td>" +
                        "<td>" + row.admin_register_date + "</td>" +
                    "</tr>"
                );
            }
            if (!$.fn.DataTable.isDataTable("#table_admins")) {
                $('#table_admins').DataTable({
                    destroy: true,
                        processing: true,
                        scrollY: "400px",
                        sScrollX: "100%",
                        sScrollXInner: "100%",
                        bScrollCollapse: true,
                        scrollCollapse: true,
                    language: {
                        lengthMenu: "Mostrando _MENU_ registros por pagina",
                        zeroRecords: "Registros no encontrados",
                        info: "Mostrando _PAGE_ paginas de _PAGES_",
                        infoEmpty: "Registros no disponibles",
                        infoFiltered: "(filtered from _MAX_ total records)"
                    }
                });
            }
            $("#table_admins").DataTable().draw();
        }
    });
}
  // LISTAR ADMINISTRADORES


function insert_new_admin(data){
    $.ajax({
        url: urlActual + "routes/?route=admin_manager_route&controller=insert_new_admin",
        type: "POST",
        dataType: "JSON",
        data: {
            data : data
        },
        beforeSend: function(){
            
        },
        success: function (response){
            Swal.fire({
                title: 'Yeei!',
                icon: 'success',
                text: 'User admin registered'
            });
            clean_form_new_admin();
            select_all_admins();
        }
    });
}


function select_specific_adminuser(id){
    $.ajax({
        url: urlActual + "routes/?route=admin_manager_route&controller=select_specific_admin",
        type: "POST",
        dataType: "JSON",
        data:{
            id:id
        },
        beforeSend: function(){
            id_edit_admin_user = "";
        },
        success: function (response){
            data = response.data;

            id_edit_admin_user = data.id_admin_user;

            $("#edit_admin_name").val(data.admin_name);
            $("#edit_admin_lastname").val(data.admin_lastname);
            $("#edit_admin_email").val(data.admin_email);
            $("#edit_admin_password").val(data.admin_password);
            $("#edit_admin_role").val(data.admin_role);
            $("#edit_admin_status").val(data.admin_status);
            $("#edit_admin_date_birth").val(data.admin_birth);
            $("#edit_admin_login_attempts").val(data.admin_attempts);
        }
    });
}

function update_specific_adminuser(data){
    $.ajax({
        url: urlActual + "routes/?route=admin_manager_route&controller=update_specific_admin",
        type: "POST",
        dataType: "JSON",
        data:{
            data : data
        },
        beforeSend: function(){
        },
        success: function (response){
            if(response.status == "ok"){
                id_edit_admin_user = "";
                $("#modal_edit_adminuser").modal("hide");
                select_all_admins();
            }
            
        }
    });
}