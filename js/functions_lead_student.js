

/* VALIDACION DEL FORM DE UN NUEVO LEAD */
$("#btn_accept").click(function(){
    var input = $('.validate-input .input100');

    var check = true;

    for(var i=0; i<input.length-1; i++) {
        if(validate(input[i]) == false){
            showValidate(input[i]);
            check=false;
        }
    }

    if(check){
        var data = {
            name : $("#name").val(),
            last_name : $("#last_name").val(),
            email : $("#email").val(),
            phone : $("#phone").val(),
            message : $("#message").val(),
        };

        register_new_lead(data);
        
    }
});

$('.validate-form .input100').each(function(){
    $(this).focus(function(){
       hideValidate(this);
    });
});

function validate (input) {
    if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
        if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
            return false;
        }
    }
    else {
        if($(input).val().trim() == ''){
            return false;
        }
    }
}

function showValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).addClass('alert-validate');
}

function hideValidate(input) {
    var thisAlert = $(input).parent();

    $(thisAlert).removeClass('alert-validate');
}

function cleanLeadForm_Student(){
    name = $("#name").val("");
    last_name = $("#last_name").val("");
    email = $("#email").val("");
    phone = $("#phone").val("");
    message = $("#message").val("");
}