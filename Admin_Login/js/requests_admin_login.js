var urlActual = "http://localhost/foreign2me_crm/Admin_Login/";
var correo_valido = "";

/* ADMIN */ 
    function validate_admin_login(data){
        $.ajax({
            url: urlActual + "routes/?route=admin_route&controller=admin_login",
			type: "POST",
            dataType: "JSON",
            data: {
                data : data
            },
            beforeSend: function(){
                
            },
            success: function (response){
                
                if(response.status == "ok"){
                    if(response.body == "User found"){
                        data = response.data[0];
                        Swal.fire({
                            title: 'Yeei!',
                            icon: 'success',
                            text: 'Login successful'
                        });

                        window.location ="../Admin_Management";
                    }else if(response.body == "Incorrect" || response.body == "No User"){
                        Swal.fire({
                            title: 'Oops!',
                            icon: 'warning',
                            text: 'Email or Password incorrect'
                        });
                    }else{
                        Swal.fire({
                            title: 'Oops!',
                            icon: 'warning',
                            text: 'You cannot access anymore'
                        });
                    }
                }else{
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: 'We are having problems, try later'
                    });
                }
                


            }
        });
    }