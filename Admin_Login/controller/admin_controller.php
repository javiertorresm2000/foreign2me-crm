<?php
    class Admins{
        private $response;
        

        public function __construct(){
			require_once '../model/Admin.php';
			$this->model = new Admin();
        }
        
        public function admin_login($data){
            $this->response	= $this->model->admin_login($data);
            
	    	echo json_encode($this->response);
        }
    }
?>