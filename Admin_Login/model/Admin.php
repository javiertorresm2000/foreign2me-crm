<?php
    class Admin{
        private $db;
        
        private $response = array(
			"status" => "",
			"body" => "",
			"data" => ""
        );

		public function __construct() {
			require_once '../config/Config_db.php';
			$class = new Connection();
			$this->db = $class->conn();
        }
        
        public function admin_login($data){
            try{
                $admin_email              = $data['admin_email'];
                $admin_password           = $data['admin_password'];

                $sql = "SELECT 
                            id_admin_user,
                            admin_email,
                            admin_password,
                            admin_status,
                            admin_attempts 
                        FROM
                            admin_user
                        WHERE 
                            admin_email = :admin_email";

                $sql = $this->db->prepare($sql);

                
                $sql->bindParam(":admin_email",$admin_email, PDO::PARAM_STR);
                $sql->execute();

                
                if($sql->rowCount() > 0){
                    /** SI EXISTE EL USUARIO, INICIAN VALIDACIONES */
                    $result = $sql->fetchAll(PDO::FETCH_ASSOC);
                    

                    /** VALIDAMOS SI ESTA ACTIVO EL ADMIN */
                    if($result[0]['admin_status'] == '1'){
                        if($result[0]['admin_password'] == $admin_password){
                            
                            $sql = "SELECT 
                                        id_admin_user,
                                        admin_email,
                                        admin_role, 
                                        admin_name,
                                        admin_lastname
                                    FROM
                                        admin_user
                                    WHERE 
                                        admin_email = :admin_email";
    
                            $sql = $this->db->prepare($sql);
                            $sql->bindParam(":admin_email",$admin_email, PDO::PARAM_STR);
                            $sql->execute();
                            $this->response['data'] = $sql->fetchAll(PDO::FETCH_ASSOC);
                            $this->response['body'] = 'User found';
                        }
                        else{
                            
                            $this->response['body'] = 'Incorrect';
                        }
                    }else{
                        $this->response['body'] = 'Inactive';
                    }
                }else{
                    $this->response['body'] = 'No User';
                }
                $this->response['status'] = 'ok';
                
            }catch (PDOException $e){
				$this->response['status'] = $e->getMessage().' LINEA['.$e->getLine().'].';
			}
			return $this->response;
        }
    }
?>